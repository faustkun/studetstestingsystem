/**
 * Created by faustkun on 15.03.15.
 */

Meteor.methods({
   itemInsert: function(itemData){
       var user = Meteor.user();
       if(Roles.userIsInRole(user._id, ['admin'])){
           var item = _.extend(itemData, {
               addedBy: user.username,
               submitted: new Date()
           });
           var itemId = Tasks.insert(item);
           return itemId;
       }
   }
});
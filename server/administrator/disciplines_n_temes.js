/**
 * Created by faustkun on 28.03.15.
 */
Meteor.methods({
    'updateTemes': function(idDisc, data){
        var user = Meteor.user();
        if(Roles.userIsInRole(user._id, ['admin'])){
            if(!duplicated(data)){
                Disciplines.update({_id: idDisc}, {$set: {temes: data}});
            }else{
                throw new Meteor.Error("Такая тема уже существует", "Такая тема уже существует");
            }

        }
    },
    "addDiscipline": function(discName){
        var user = Meteor.user();
        if(Roles.userIsInRole(user._id, ['admin'])){
            if(Disciplines.findOne({name: discName}) == undefined){
                var discipline = {
                    name: discName,
                    temes: []
                };
                Disciplines.insert(discipline);
            } else{
                throw new Meteor.Error(" Такая дисциплина уже существует", "Такая дисциплина уже существует");
            }

        }
    },
    "deleteDiscipline": function(discId){
        var user = Meteor.user();
        if(Roles.userIsInRole(user._id, ['admin'])){
            Disciplines.remove({_id: discId});
        }
    }
});


function duplicate (a, s){
    for(var i= a.indexOf(s) + 1; i < a.length; i ++){
        if(a[i] == s){return true}
    }
    return false
}

function duplicated(a){
    for(var i = 0;i < a.length; i++){
        if(duplicate(a, a[i])){return true}
    }
    return false;
}
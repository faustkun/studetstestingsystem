/**
 * Created by faustkun on 31.03.15.
 */
Template.taskItem6.rendered = function(){
    var t = Template.instance().$('.text-case');
    var tt = t[t.length - 1 ];
    tt.remove();
};


Template.taskItem6.events({
    'click .delete': function(){if(confirm('Вы уверены что хотите удалить это тестовое задание?')){
        Meteor.call('deleteTask', this._id, function(error){
            if(error){throwError(error.reason);}
            else{throwNotification("Тестовое задание успешно удалено");}});
    }
    }
});
/**
 * Created by faustkun on 20.03.15.
 */
var first_len = 2;
var second_len = 2;

Template.newAccordItem.rendered = function(){
    checkNumber1 = function (e) {
        var s = parseInt(e.target.value + String.fromCharCode(e.charCode), 10);
        if(e.target.max < s || e.target.min > s){
            return false;
        }
        return (/[0-9]/.test(String.fromCharCode(e.charCode)));};
    checkNumber2 = function(e){
        var s = parseInt(e.target.value, 10);
        //console.log(s);
        if(e.target.max < s || e.target.min > s){
            e.target.value = 1;
            return false;
        }
    };
    checkTextarea1 = function(e){
        //console.log(e);
        var maxLen = 800;
        if (e.target.value.length > maxLen){
            e.target.value = e.target.value.substr(0, maxLen);
        }
    };
    checkTextarea2 = function(e){
        //console.log(e);
        var maxLen = 800;
        if (e.target.value.length == maxLen){
            return false;
        }
    };

    var t = Template.instance().$('input[type="number"]');
    for (var i =0; i < t.length; i++){
        t[i].onkeypress = checkNumber1;
        t[i].onchange = checkNumber2;
    }

    var t = Template.instance().$('textarea');
    for (var i =0; i < t.length; i++){
        t[i].onchange = checkTextarea1;
        t[i].onkeypress = checkTextarea2;
    }
};
Template.newAccordItem.events({
    'click input[type="submit"]': function () {
        var file = Template.instance().$('#file').get(0).files[0];
        var fileObj = Images.insert(file);
        Template.instance().image_id = fileObj._id;
        console.log(fileObj._id);
    },
    'click .accord_f_inc': function(){
        var parent = document.getElementById("accord_first_list");
        var elem = document.createElement('div');
        first_len ++;
        elem.id = 'accord_f' + first_len;
        var e1 = "<input type='text' class='form-control' maxlength='200' id='accord_f_t_"+ first_len + "'>";
        var e2 = "<input type='number' min='1' max='" + second_len +"' value='1' id='accord_f_n_" +
            first_len + "' class='numberList'>";
        elem.innerHTML = e1 + e2;
        parent.appendChild(elem);
    },
    'click .accord_f_dec': function(){
        if (first_len > 1){
            var elem = document.getElementById('accord_f' + first_len);
            first_len--;
            elem.remove();
        }
    },
    'click .accord_s_inc': function(){
        var parent = document.getElementById("accord_second_list");
        var elem = document.createElement('div');
        second_len ++;
        elem.id = 'accord_s' + second_len;
        elem.innerHTML = "<input type='text' class='form-control' maxlength='200' id='accord_s_t_" + second_len +"'>";
        parent.appendChild(elem);
        for (var i = 1; i < first_len + 1; i++){
            var t = document.getElementById("accord_f_n_" + i);
            t.max = second_len + '';

        }
    },
    'click .accord_s_dec': function(){
        if (second_len > 1){
            var elem = document.getElementById('accord_s' + second_len);
            second_len--;
            elem.remove();

            for (var i = 1; i < first_len + 1; i++){
                var t = document.getElementById("accord_f_n_" + i);
                t.max = second_len + '';
                if(t.value > second_len){t.value = "1"}
            }
        }
    },
    'click .accord_item_adding': function(){
        var discipline = Disciplines.find({_id: Template.instance().$('.discip')[0].value}).fetch()[0];
        var teme = Template.instance().$('.tem')[0].value;

        var head = document.getElementById('accord_head').value;
        var url = '';
        if (Template.instance().image_id != undefined){
            url = '/cfs/files/img/' + Template.instance().image_id;
        }
        var first = [];
        var second = [];
        var answers = [];
        for(var i = 1; i < first_len + 1; i ++){
            first[i - 1] = document.getElementById('accord_f_t_' + i).value;
            answers[i -1] = parseInt(document.getElementById('accord_f_n_' + i).value, 10);
        }
        for(var i = 1; i < second_len + 1; i ++){
            second[i - 1] = document.getElementById('accord_s_t_' + i).value;
        }
        var cost = parseInt(document.getElementById('accord_cost').value, 10);


        var item = {
            type: 3,
            discipline: discipline,
            teme: teme,
            head: head,
            url: url,
            imgId: Template.instance().image_id,
            first: first,
            second: second,
            answers: answers,
            cost: cost
        };

        Meteor.call("itemInsert",item, function(error){
            if(error){throwError(error.reason);}
            else{throwNotification("Тестовое задание успешно добавлено в БД");}});

        //console.log(item);

    }
});
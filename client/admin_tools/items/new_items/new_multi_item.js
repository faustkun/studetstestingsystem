/**
 * Created by faustkun on 16.03.15.
 */
var len = 4;
Template.newMultiItem.rendered = function(){
    checkNumber1 = function (e) {
        var s = parseInt(e.target.value + String.fromCharCode(e.charCode), 10);
        if(e.target.max < s || e.target.min > s){
            return false;
        }
        return (/[0-9]/.test(String.fromCharCode(e.charCode)));};
    checkNumber2 = function(e){
        var s = parseInt(e.target.value, 10);
        //console.log(s);
        if(e.target.max < s || e.target.min > s){
            e.target.value = 1;
            return false;
        }
    };
    checkTextarea1 = function(e){
        //console.log(e);
        var maxLen = 800;
        if (e.target.value.length > maxLen){
            e.target.value = e.target.value.substr(0, maxLen);
        }
    };
    checkTextarea2 = function(e){
        //console.log(e);
        var maxLen = 800;
        if (e.target.value.length == maxLen){
            return false;
        }
    };

    var t = Template.instance().$('input[type="number"]');
    for (var i =0; i < t.length; i++){
        t[i].onkeypress = checkNumber1;
        t[i].onchange = checkNumber2;
    }

    var t = Template.instance().$('textarea');
    for (var i =0; i < t.length; i++){
        t[i].onchange = checkTextarea1;
        t[i].onkeypress = checkTextarea2;
    }
};

Template.newMultiItem.events({
    'click input[type="submit"]': function () {
        var file = Template.instance().$('#file').get(0).files[0];
        var fileObj = Images.insert(file);
        Template.instance().image_id = fileObj._id;
        //console.log(fileObj._id);
    },
    'click .multy_inc': function(){
        var parent = document.getElementById("multi_variants");
        var elem = document.createElement('div');
        len ++;
        elem.id = 'multy' + len;
        elem.innerHTML = "<input type='text' class='form-control' maxlength='200' id='multy_t_" + len + "'> <input type='checkbox' id='multy_c_" + len + "'>";
        parent.appendChild(elem);
        },
    'click .multy_dec': function(){
        if (len > 1){
            var elem = document.getElementById('multy' + len);
            len --;
            elem.remove();
        }
    },//input[type="submit"]
    'click .multy_item_adding': function(){
        var discipline = Disciplines.find({_id: Template.instance().$('.discip')[0].value}).fetch()[0];
        var teme = Template.instance().$('.tem')[0].value;

        var head = document.getElementById('multy_head').value;
        var url = '';
        if (Template.instance().image_id != undefined){
            url = '/cfs/files/img/' + Template.instance().image_id;
        }
        var data = [];
        var answers = [];
        for(var i = 1; i < len + 1; i ++){
            data[i - 1] = document.getElementById('multy_t_' + i).value;
            if(document.getElementById('multy_c_' + i).checked){answers[answers.length] = i;}
        }
        var cost = parseInt(document.getElementById('multy_cost').value, 10);
        var item = {
            type: 1,
            discipline: discipline,
            teme: teme,
            url: url,
            imgId: Template.instance().image_id,
            head: head,
            data: data,
            answers: answers,
            cost: cost
        };

        Meteor.call("itemInsert",item, function(error){
                                            if(error){throwError(error.reason);}
                                            else{throwNotification("Тестовое задание успешно добавлено в БД");}});

        //console.log(item);
    },
    'click .test': function(){
        var discipline = Disciplines.find({_id: Template.instance().$('.discip')[0].value}).fetch()[0];
        var teme = Template.instance().$('.tem')[0].value;

        console.log(discipline, "  ", teme);
    }
});


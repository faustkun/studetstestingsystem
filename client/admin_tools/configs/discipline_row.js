/**
 * Created by faustkun on 28.03.15.
 */
Template.disciplineRow.events({
    'click .delete_teme': function(){
        if(confirm("Внимание! Удаление темы повлечет за собой " +
            "удаление всех тестовых заданий и тестов по этой теме")){
            var data = Template.instance().data.temes.slice();
            if(find(data, this) >= 0){
                data.splice(find(data, this), 1);
            }
            Meteor.call('updateTemes', Template.instance().data._id, data, function(error){
                if(error){throwError(error.reason);}
                else{throwNotification("Тема успешно удалена");}});
        }
    },
    'click .delete_discipline': function(){
        if(confirm("Внимание! Удаление дисциплины повлечет за собой " +
            "удаление всех тестовых заданий по этой дисциплине, а также " +
            "тестов и прочих связанных данных")){
            Meteor.call('deleteDiscipline', this._id, function(error){
                if(error){throwError(error.reason);}
                else{throwNotification("Дисциплина успешно удалена из БД");}});
        }

    },
    'click .check_new_teme': function(){
        if(Template.instance().$(".new_teme")[0].value != ""){
            var data = Template.instance().data.temes.slice();
            data[data.length] = Template.instance().$(".new_teme")[0].value;
            Meteor.call('updateTemes', Template.instance().data._id, data, function(error){
                if(error){throwError(error.reason);}
                else{throwNotification("Тема успешно добавлена в БД");}});
        } else {
            throwError("Слишком короткое имя темы");
        }

    }
});

function find(arr, s){
    for(var i = 0; i <arr.length; i++){
        if(arr[i] == s){return i;}
    }
    return -1;
};
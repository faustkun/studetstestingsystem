/**
 * Created by faustkun on 28.03.15.
 */
Template.groupRow.events({
    'click .delete_group': function(){
        Session.set("deletingGroup", this.name);
        if(confirm("Внимание! Удаление группы пользователей повлечет за собой удаление " +
            "всех пользователей входящих в эту группу и всех пройденных ими тестов")){
            Meteor.call("deleteGroup", this._id, function(error){
                if(error){throwError(error.reason);}
                else{throwNotification("Группа пользователей " + Session.get('deletingGroup') + " успешно удалена");}});
        }

    }
});
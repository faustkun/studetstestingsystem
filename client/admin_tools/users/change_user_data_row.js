/**
 * Created by faustkun on 27.03.15.
 */
Template.changeUserDataRow.rendered = function(){
    var select = Template.instance().$('.change_user_data_4')[0].options;
    //console.log(Template.instance().data.profile.group._id);
    for(var i = 0; i < select.length; i++){
        if(select[i].value == Template.instance().data.profile.group._id){
            select[i].selected = true;}
    }
};

Template.changeUserDataRow.events({
    'click .edit': function(){
        var inst = Template.instance();
        var edited = {
            username: inst.$('.change_user_data_2')[0].value,
            profile: {
                name: inst.$('.change_user_data_3')[0].value,
                studentNumber: inst.$('.change_user_data_1')[0].value,
                group: Groups.find({_id: inst.$('.change_user_data_4')[0].value}).fetch()[0]
            }
        };
        //console.log(this._id, edited);

        Meteor.call('updateUserData', this._id, edited, function(error){
            if(error){throwError(error.reason);}
            else{throwNotification("Данные пользователя успешно изменены");}});
    },

    'click .delete': function(){
        //console.log(this._id);
        if(confirm('Вы уверены что хотите удалить этого пользователя?')){
            Meteor.call("deleteUser", this._id, function(error){
                if(error){throwError(error.reason);}
                else{throwNotification("Пользователь успешно удален из БД");}});
        }

    },

    'click .changeUserPassword': function(){
        Session.set('changedUser', this.username);
        Meteor.call('changePasswd', this._id, "1111",  function(error){
            if(error){throwError(error.reason);}
            else{throwNotification("Пароль пользователя " + Session.get('changedUser') + " успешно сброшен на 1111");}});
    }
});
var mass;
var mass2;
var stop = 0;
var work = 0;
var block = 0;
var n = 40;
var color_zero = '#eeeeee';
var color_one = '#125bb2';
var width = 750;
var height = 750;
var wait = 100;
var temp1 = [];
var temp2 = [];


var data =
    [[7, 7, 7, 7, 8, 9, 10, 10, 10, 10, 10, 10, 10, 12, 12, 12, 12, 12, 13, 14, 15, 15, 15, 15,
        15, 14, 13, 17, 17, 17, 17, 18, 19, 20, 20, 20, 20, 20, 20, 20, 7, 7, 7, 7, 7, 7, 7, 8,
        9, 10, 10, 9, 8, 12, 12, 12, 12, 12, 12, 13, 14, 15, 15, 15, 15, 15, 15, 13, 14, 20, 19,
        18, 17, 17, 17, 17, 17, 18, 19, 20, 20, 19, 22, 22, 22, 22, 22, 22, 22, 23, 24, 25, 23,
        24, 23, 24, 25, 7, 7, 7, 7, 7, 7, 7, 10, 10, 10, 10, 10, 10, 10, 8, 8, 9, 9, 12, 12, 12,
        12, 12, 13, 14, 15, 15, 15, 15, 15, 14, 13, 17, 18, 19, 20, 21, 19, 19, 19, 19, 19, 19,
        7, 7, 7, 7, 7, 7, 7, 8, 9, 10, 8, 9, 12, 12, 12, 12, 12, 13, 14, 15, 15, 15, 15, 15, 14,
        13, 17, 17, 17, 17, 17, 17, 18, 19, 20, 20, 20, 20, 20, 20, 22, 22, 22, 22, 22, 22, 22,
        25, 25, 25, 25, 25, 25, 25, 23, 23, 24, 24, 27, 27, 27, 27, 27, 27, 27, 28, 29, 30, 30,
        30, 30, 30, 29, 28],
    [2, 3, 4, 5, 5, 5, 5, 4, 3, 2, 6, 7, 8, 3, 4, 5, 6, 7, 8, 8, 7, 6, 5, 4, 3, 2, 2, 2, 3, 4,
        5, 5, 5, 5, 4, 3, 2, 6, 7, 8, 10, 11, 12, 13, 14, 15, 16, 10, 10, 11, 12, 13, 13, 16, 15,
        14, 13, 12, 11, 10, 10, 11, 12, 13, 14, 15, 16, 14, 14, 11, 10, 10, 11, 12, 13, 14, 15,
        16, 16, 15, 14, 14, 10, 11, 12, 13, 14, 15, 16, 10, 10, 10, 13, 13, 16, 16, 16, 18, 19,
        20, 21, 22, 23, 24, 18, 19, 20, 21, 22, 23, 24, 20, 21, 22, 23, 19, 20, 21, 22, 23, 24,
        24, 23, 22, 21, 20, 19, 18, 18, 18, 18, 18, 18, 18, 19, 20, 21, 22, 23, 24, 26, 27, 28,
        29, 30, 31, 32, 26, 26, 26, 29, 29, 27, 28, 29, 30, 31, 32, 32, 31, 30, 29, 28, 27, 26,
        26, 26, 27, 28, 29, 30, 31, 32, 32, 31, 30, 29, 28, 27, 26, 26, 27, 28, 29, 30, 31, 32,
        26, 27, 28, 29, 30, 31, 32, 28, 29, 30, 31, 26, 27, 28, 29, 30, 31, 32, 26, 26, 27, 28,
        29, 30, 31, 32, 32]]


Template.error2.rendered = function(){
    if(!this._rendered){
        this._rendered = true;
    }
    //инициализация слайдера
    $('.sliders').noUiSlider({
        start: 100,
        collect: 'lover',
        orientation: 'horizontal',
        range: {
            'min': 10,
            'max': 1000
        },
        format: wNumb({
            decimals: 0
        })
    });
    $('.sliders').on('slide', function(){wait = $('#sli').val();});

    //инициируем канвас
    var canvas = document.getElementById('canvas');
    var shift_x = width / n | 0;
    var shift_y = height / n | 0;

    canvas.width = shift_x * n + n + 1;
    canvas.height = shift_y * n + n + 1;
    var context = canvas.getContext('2d');
    context.fillStyle = '#cccccc';
    context.fillRect(0, 0, canvas.width, canvas.height);

    context.fillStyle = color_zero;


    context.fillRect(1, 1, shift_x, shift_y);
    context.fillRect(1, shift_y + 2, shift_x, shift_y);

    for(var i = 0; i < n; i ++){
        for(var j = 0; j < n; j ++){
            context.fillRect(1 + i * (shift_x + 1), 1 + j * (shift_y + 1), shift_x, shift_y);
        }
    }

    //создаём массивы для хранения состояний
    mass = [];
    mass2 = [];
    for(var i = 0; i<n; i++) {
        mass[i] = [];
        mass2[i] = [];
        for (var j = 0; j < n; j++) {
            mass[i][j] = 0;
            mass2[i][j] = 0;
        }
    }


    //пишем ошибку квадратиками
    context.fillStyle = color_one;
    for(var i = 0; i < data[0].length; i++){
        context.fillRect(1 + data[0][i] * (shift_x + 1), 1 + data[1][i] * (shift_y + 1), shift_x, shift_y);
        mass[data[0][i]][data[1][i]] = 1;
    }
    setTimeout(function(){document.getElementById('bb').click();}, 5000);

    //вешаем эвент на канвас который будет закрашивать квадратики
    canvas.addEventListener('click', function(e){
        var shift_x = width / n | 0;
        var shift_y = height / n | 0;
        var ctx = canvas.getContext('2d');
        var x = (e.pageX - canvas.offsetLeft) / (shift_x + 1) | 0;
        var y = (e.pageY - canvas.offsetTop) / (shift_y + 1) | 0;

        if (mass[x][y] == 0){
            ctx.fillStyle = color_one;
            mass[x][y] = 1}
        else{
            ctx.fillStyle = color_zero;
            mass[x][y] = 0;
        }
        ctx.fillRect(1 + x * (shift_x + 1), 1 + y * (shift_y + 1), shift_x, shift_y);
        temp1[temp1.length] = x;
        temp2[temp2.length] = y;

    }, false);

}


Template.error2.events({
    'click .stop': function(){
        stop = 1;
        block = 0;

        /*var s1 = '[';
        var s2 = '[';
        for(var i = 0; i < temp1.length; i++){
            s1 = s1 + temp1[i] + ', ';
            s2 = s2 + temp2[i] + ', ';
        }
        s1 = s1 + ']';
        s2 = s2 + ']';
        console.log(s1);
        console.log(s2);*/
    },
    'click .go': function(){
        if(!block){
            block = 1;
            work = 1;
            stop = 0;
            //создаём новый массив, для хранения результатов
            function live(){
                work = 0;
                delete mass2;
                mass2 = [];
                for(var i = 0; i<n; i++) {
                    mass2[i] = [];
                    for (var j = 0; j < n; j++) {
                        mass2[i][j] = mass[i][j];
                    }
                }
                for(var i = 0; i < n; i ++){
                    for(var j = 0; j < n; j ++){
                        //расчет количества живых соседей
                        var count = 0;
                        for(var ii = i - 1; (ii < n) && (ii < i + 2); ii ++){
                            for(var jj = j - 1; (jj < n) && (jj < j + 2); jj ++){

                                if((ii != -1) && (jj != -1)){
                                    count = count + mass[ii][jj];
                                }
                            }
                        }
                        count = count - mass[i][j];
                        //отработка правил игры жизни
                        if((mass[i][j] == 0) && (count == 3)){mass2[i][j] = 1; work = 1;}
                        if((mass[i][j] == 1) && ((count < 2) || (count > 3))){mass2[i][j] = 0; work = 1;}
                    }
                }
                delete mass;
                mass = mass2;
                //Отрисовка
                var canvas = document.getElementById('canvas');
                var context = canvas.getContext('2d');
                var shift_x = width / n | 0;
                var shift_y = height / n | 0;

                for(var i = 0; i < n; i ++){
                    for(var j = 0; j < n; j ++){
                        if(mass[i][j] == 0){context.fillStyle = color_zero;}
                        else {context.fillStyle = color_one;}
                        context.fillRect(1 + i * (shift_x + 1), 1 + j * (shift_y + 1), shift_x, shift_y);
                    }
                }

                if(work && !stop){setTimeout(live, wait);}
                else{block = 0;
                    console.log("stopped");}
            }
            setTimeout(live, wait);
        }
    },
    'click .clear': function(){
        stop = 1;
        console.log("CLEAR");
        delete  mass;
        delete mass2;
        mass = [];
        mass2 = [];
        for(var i = 0; i<n; i++) {
            mass[i] = [];
            mass2[i] = [];
            for (var j = 0; j < n; j++) {
                mass[i][j] = 0;
                mass2[i][j] = 0;
            }
        }

        var shift_x = width / n | 0;
        var shift_y = height / n | 0;
        var canvas = document.getElementById('canvas');
        var context = canvas.getContext('2d');
        context.fillStyle = color_zero;

        for(var i = 0; i < n; i ++){
            for(var j = 0; j < n; j ++){
                context.fillRect(1 + i * (shift_x + 1), 1 + j * (shift_y + 1), shift_x, shift_y);
            }
        }
    },
    'click .random': function(){
        stop = 1;
        console.log("RANDOM");
        delete  mass;
        delete mass2;
        mass = [];
        mass2 = [];
        for(var i = 0; i<n; i++) {
            mass[i] = [];
            mass2[i] = [];
            for (var j = 0; j < n; j++) {
                var temp = Math.random();
                if(temp< 0.7){temp = 0}
                else{temp = 1}
                mass[i][j] = temp;
                mass2[i][j] = temp;
            }
        }

        var canvas = document.getElementById('canvas');
        var context = canvas.getContext('2d');
        var shift_x = width / n | 0;
        var shift_y = height / n | 0;

        for(var i = 0; i < n; i ++){
            for(var j = 0; j < n; j ++){
                if(mass[i][j] == 0){context.fillStyle = color_zero;}
                else {context.fillStyle = color_one;}
                context.fillRect(1 + i * (shift_x + 1), 1 + j * (shift_y + 1), shift_x, shift_y);
            }
        }
    }

})
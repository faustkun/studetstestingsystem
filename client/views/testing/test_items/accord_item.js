/**
 * Created by faustkun on 04.04.15.
 */
Template.accordItem.helpers({
    second_length: function(){return Template.instance().data.second.length;}
});

Template.accordItem.events({
    /*'blur input': function(){
        var temp = Template.instance().$('.first input');
        var result = [];
        for(var i = 0; i < temp.length; i ++){
            result[i] = parseInt(temp[i].value, 10);
        }
        console.log(result, Template.instance().$('.first input'));
        Session.set(Template.instance().data._id, result);
    },*/
    'change input': function(){
        var temp = Template.instance().$('.first input');
        var result = [];
        for(var i = 0; i < temp.length; i ++){
            result[i] = parseInt(temp[i].value, 10);
        }
        //console.log(result, Template.instance().$('.first input'));
        Session.set(Template.instance().data._id, result);
    }
});

Template.accordItem.rendered = function(){
    var temp = Template.instance().$('.first input');
    var result = [];
    for(var i = 0; i < temp.length; i ++){
        result[i] = parseInt(temp[i].value, 10);
    }
    checkNumber1 = function (e) {
        //console.log("asdasd");
        var s = parseInt(e.target.value + String.fromCharCode(e.charCode), 10);
        if(e.target.max < s || e.target.min > s){
            return false;
        }
        return (/[0-9]/.test(String.fromCharCode(e.charCode)));};
    checkNumber2 = function(e){
        var s = parseInt(e.target.value, 10);
        //console.log(s);
        if(e.target.max < s || e.target.min > s){
            e.target.value = 1;
            return false;
        }
    };

    var t = Template.instance().$('input[type="number"]');
    for (var i =0; i < t.length; i++){
        t[i].onkeypress = checkNumber1;
        t[i].onchange = checkNumber2;
    }
    //console.log(result, Template.instance().data._id);
    Session.set(Template.instance().data._id, result);
};
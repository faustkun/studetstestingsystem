/**
 * Created by faustkun on 04.04.15.
 */
Template.freeItem.events({
    'blur textarea': function(){
        var result = Template.instance().$('textarea')[0].value;
        //console.log(result);
        Session.set(Template.instance().data._id, result);
    }
});

Template.freeItem.rendered = function(){
    var result = Template.instance().$('textarea')[0].value;
    //console.log(result);
    Session.set(Template.instance().data._id, result);

    checkTextarea1 = function(e){
        //console.log(e);
        var maxLen = 800;
        if (e.target.value.length > maxLen){
            e.target.value = e.target.value.substr(0, maxLen);
        }
    };
    checkTextarea2 = function(e){
        //console.log(e);
        var maxLen = 800;
        if (e.target.value.length == maxLen){
            return false;
        }
    };

    var t = Template.instance().$('textarea');
    for (var i =0; i < t.length; i++){
        t[i].onchange = checkTextarea1;
        t[i].onkeypress = checkTextarea2;
    }
};
/**
 * Created by faustkun on 04.04.15.
 */
Template.addItem.rendered = function(){
    var t = Template.instance().$('.add_form');
    var tt = t[t.length - 1 ];
    tt.remove();


    var temp = Template.instance().$("input");
    var result = [];
    for(var i = 0; i< temp.length; i ++){
        result[i] = temp[i].value;
    }

    //console.log(result);
    Session.set(Template.instance().data._id, result);
};

Template.addItem.events({
    'blur input': function(){
        var temp = Template.instance().$("input");
        var result = [];
        for(var i = 0; i< temp.length; i ++){
            result[i] = temp[i].value;
        }

        //console.log(result);
        Session.set(Template.instance().data._id, result);
    }
});

